import { customElement, html, LitElement, property } from "lit-element";
import { emit, getState, setState } from "./util";
import { distinctUntilChanged, map, pluck, tap } from 'rxjs/operators'
import { Subscription } from "rxjs";

@customElement('test-component')
export class TestComponent extends LitElement {
  @property({ type: String }) name: string = '';
  @property({ type: Number }) counter: number = 0;
  subscriptions = new Subscription();

  // once the component has been added to the DOM, append the "subscribes" to the subscription list
  connectedCallback() {
    super.connectedCallback();
    const sub = getState().pipe(
      pluck('counter'),
      map((counter: number) => Number.isNaN(counter) ? 0 : counter + 1),
      distinctUntilChanged()
    ).subscribe(counter => this.counter = counter)

    this.subscriptions.add(sub);
  }

  // once the component has been destroyed, unsubscribe from the list of subscriptions
  disconnectedCallback() {
    this.subscriptions.unsubscribe();
    super.disconnectedCallback();
  }

  render() {
    return html`
      <div>
        <p @click=${this.onClick}>${this.name.length ? html`${this.name}` : 'Loading...'}</p>
        ${this.counter ? html`<p>You have clicked on me ${this.counter} time(s).</p>` : ''}
      </div>
    `;
  }

  // ------------------------------------------------------------------------

  onClick() {
    setState({ counter: this.counter });

    // composition over inheritance
    emit('title-click', 'My Application Test', this);
  }
}
